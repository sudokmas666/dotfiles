#!/bin/bash
lsblk -l | awk '{print $1,$4}' | grep "sd" | dmenu -l 20 |  awk '{print $1}' |xargs -I % sudo mount /dev/% ~/sdcard
